n = int(input("Digite algum número, apenas inteiros: "))

contador = 0

#o for repete a quantidade da variavel n, que é divisida por i
for i in range(1, n + 1):
    if n % i == 0:
        contador += 1

if contador == 2:
    print("O número é primo")
else:
    print("O número não é primo")
