lugares = ['japão', 'estados unidos', 'canada', 'caribe', 'bora bora']

print('\nLista original:',lugares)

print('\nLista em ordem alfabetica:',sorted(lugares))

print('\nLista original:',lugares)

lugares.reverse()

print('\nLista alterada',lugares)

lugares.reverse()
print('\nLista de volta ao original:', lugares)

lugares.sort()
print('\nOrdem alterada:',lugares)

lugares.sort(reverse=True)
print('\nInvertido:',lugares)