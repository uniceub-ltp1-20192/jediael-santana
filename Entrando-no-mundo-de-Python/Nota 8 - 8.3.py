menu = ['pizza', 'camarao', 'picanha', 'hamburguer', 'cachorro quente', 'lasanha', 'açaí', 'sorvete', 'milk shake', 'batata frita']

print('original >',menu)

menu.sort()
print('\nordem alterada >',menu)

menu.sort(reverse=True)
print('\ninverso >',menu)

print('\nlista ordenada >',sorted(menu))

sorted(menu, reverse=True)
print('\nlista alterada >', menu)

menu.reverse()
print('\nreverse >', menu)

menu.reverse()
print('\nreverse, como estava >', menu)

print('\nitens que tem no menu >',len(menu))