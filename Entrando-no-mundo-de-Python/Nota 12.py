buffet = ('camarão', 'salgado', 'linguiça', 'torta', 'picanha')
print("Menu:")

for menu in buffet:
    print('>', menu.title())

#del buffet[0] > TypeError: 'tuple' object doesn't support item deletion

buffet = ('camarão', 'suchi', 'bacalhau', 'torta', 'picanha')
print('\nMenu atualizado: ')

for z in buffet:
    print('>', z.title())