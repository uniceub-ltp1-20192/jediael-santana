pizza_hut = ['calabreza', 'banana', 'chocolate', 'frango', 'portuguesa', 'quatro queijos']
print('Pizzaria menu:')

for z in pizza_hut[:5]:
    print('>',z.title())

herois = ['spider-man', 'iron man', 'thor', 'capitain america', 'hulk', 'black window', 'hawkeye', 'war machine', 'falcon', "scarlet witch's", "vision's", 'ant-man', 'the wasp', 'black panther', 'doctor Strange', 'capitain marvel']
print('\nVingadores:')

for hero in herois[:12]:
    print('>', hero.title())

churrasco = ['picanha', 'maminha', 'coração', 'linguiça', 'frango', 'cupin']
print('\nChurrascaria menu:')

for churras in churrasco:
    print('>', churras)
