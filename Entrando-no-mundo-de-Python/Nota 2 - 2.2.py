#a variavel cebolas possui um valor igual a 300
cebolas = 300

#a variavel cebolas_na_caixa possui um valor igual a 120
cebolas_na_caixa = 120

#a variavel espaco_caixa possui um valor igual a 5
espaco_caixa = 5

#a variavel caixas possui um valor igual a 60
caixas = 60

#a variavel cebolas_fora_da_caixa possui resultado de uma subtração entre as outras duas variaveis
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa

#a variavel caixas_vazias paossui o resultado de divisão e subtração entre as tres variaveis
caixas_vazias = caixas - (cebolas_na_caixa / espaco_caixa)

#a variavel caixas_necessarias possui o resultado da divisão entre as duas variaveis
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa

#aqui printa na tela a string e o valor da variavel
print ("Existem", cebolas_na_caixa, "cebolas encaixotadas")

#aqui printa na tela a string e o valor da variavel
print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")

#aqui printa na tela a string e o valor da variavel
print ('Em cada caixa cabem', espaco_caixa, 'cebolas')

#aqui printa na tela a string e o valor da variavel
print ('Ainda temos', caixas_vazias, 'caixas vazias')

#aqui printa na tela a string e o valor da variavel
print ('Então, precisamos de', caixas_necessarias, 'caixas para empacotar todas as cebolas')