import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Maioridade {
	public static void main(String args[]) throws Exception{
		//Declaracao de variaveis
		
		int usuario = 0;
		int cabin = 1;
		int vamigos = 0;
		float valtura = 0;
		String vnome = "";
		
		//Entrada de dados
		vnome = ValorNome();
		valtura = ValorAltura();
		vamigos = ValorAmigos();
		
		
		cabin = ValidadeDados(vnome, valtura, vamigos, usuario);
		//CheckPremission(valtura, vamigos);
	}
	
	
	static String ValorNome() throws Exception{
    	
    	String nome = "";
    	
    	System.out.print("Digite seu nome completo: ");
    	nome = (new BufferedReader(new InputStreamReader(System.in))).readLine();
    	
    	return nome;
    	
    }   
    
    static float ValorAltura() throws Exception {
    	String leitor = "";
    	
    	System.out.print("Digite sua altura: ");
    	leitor = (new BufferedReader(new InputStreamReader(System.in))).readLine();
    	
    	return Float.parseFloat(leitor);	
    
    }
    
    static int ValorAmigos() throws Exception {
    	String amigos = "";
    
    	System.out.print("Digite quantos amigos: ");
    	amigos = (new BufferedReader(new InputStreamReader(System.in))).readLine();
    	
    	return Integer.parseInt(amigos);

    }
    
    static int ValidadeDados(String vnome, float valtura, int vamigos, int usuario) throws Exception {
    	//metodo de comparação 
    	if(vnome.length() < 9){ 
    	System.out.println("Digite seu nome completo, pf.");
    	System.exit(0);
    	}
    	if(valtura < 1.50){
    	System.out.println("Você é muito baixo, lamento.");
    	System.exit(0);
    	}
    	if(valtura > 2.40){
    	System.out.println("Você é muito alto! Lamento.");
    	System.exit(0);
        }
    	if(vamigos > 9){
        	System.out.println("Desculpa, mas tem muita gente.");
        	System.exit(0);
           }
    	if(vamigos < 1){
        	System.out.println("Você não pode entrar sozinho, arranja um amigo(a).");
        	System.exit(0);
           }
		return usuario;
    	
     }
   

}